#作者：道长

import pytest
import  sys
sys.path.append("..")
from common.db import *
from common.read_yaml import ReadYamlConfigs
from common.logintoken import ZhangSanLogin,LiSiLogin
from api.admin.membercontroller_api import MemberControllerApi
from api.admin.receiving_api import ReceivingApi
from common.readCofig import zhangsan_token,lisi_token
data = ReadYamlConfigs("data.yml").get_yaml_data()#读取数据




"""-----------------调用api并且进行token身份切换-----------------"""

"""张三模块"""
@pytest.fixture(scope="session")
def membercontroller_z():
    return MemberControllerApi(zhangsan_token)

@pytest.fixture(scope="session")
def receiving_z():
    return ReceivingApi(zhangsan_token)

"""李四模块"""
@pytest.fixture(scope="session")
def membercontroller_l():
    return MemberControllerApi(lisi_token)

@pytest.fixture(scope="session")
def receiving_l():
    return ReceivingApi(lisi_token)


"""-------------------进行初始化登录----------------------"""
@pytest.fixture(scope="session",autouse=True)
def zhangsan_login(zhangsan_info):
    return ZhangSanLogin(zhangsan_info,'zhangsan_token')

@pytest.fixture(scope="session",autouse=True)
def lisi_login(lisi_info):
    return LiSiLogin(lisi_info,'lisi_token')


"""----------------获取账号数据----------------------------"""
@pytest.fixture(scope="session")
def zhangsan_info():
    return data['zhangsan']

@pytest.fixture(scope="session")
def lisi_info():
    return data['lisi']




# # -----------------------------------第一，第二,第三个人登录的session---------------------------
# @pytest.fixture(scope="session")
# def first_session(first_action):
#     return first_action.session
# @pytest.fixture(scope="session")
# def second_session(second_action):
#     return second_action.session
# @pytest.fixture(scope="session")
# def three_session(three_action):
#     return three_action.session
#
# @pytest.fixture(scope="session")
# def admin_three_session(admin_action):
#     return admin_action.session
# def pytest_sessionfinish(session):
#     '''
#     用例执行完毕后，生成allure报告
#     :param session:
#     :return:
#     '''
#     base_dir = get_base_dir()
#     allure_report_dir = f"{base_dir}\\allure_reports"
#     from_report_dir = f"{base_dir}\\reports"
#     os.system(f"copy {base_dir}\\configs\\environment.properties {from_report_dir}")  # 拷贝配置文件至allure报告路径中
#     os.system(f"allure generate {from_report_dir} -o {allure_report_dir} --clean")  # 生成allure报告
#     os.system(f"allure open {allure_report_dir}")  # 打开报告
