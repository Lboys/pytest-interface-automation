import allure
from case.conftest import *
from common.assert_api import assert_api
from common.read_yaml import *
from common.logger import Logger
testdata = ReadYamlData('dan_data/admin_data/login_data.yml').get_yaml_data()#读取数据
data =testdata['test_login_data']
@allure.feature('登录测试用例接口')#测试报告显示测试功能
class Test_login():
    '''测试登录接口'''
    # log = Log()
    @pytest.mark.parametrize("username,password,expect",data,
                            ids = ["正常登录",
                                   "密码为空登录",
                                   # "账号为空登录",
                                   # "账号错误登录",
                                   # "密码错误登录",
                                   # "账号存在空格登录",
                                   # "密码存在空格登录",
                                   # "账号存在特殊符号登录",
                                   # "密码存在特殊符号登录",
                                   # "账号不完整登录",
                                   # "密码不完整登录"
                                    ])#参数化测试用例
    @allure.step('账号，密码登录')#测试报告显示步骤
    def test_login(self,username,password,expect,membercontroller_z):
        ##登录
        login = membercontroller_z.login(password=password
                       ,username=username)
        print("passwordpasswordpasswordpassword",password)
        print("loginloginloginloginlogin",login)
        assert_api(actual_code=login['code'],expect_code=expect['code']
                   ,actual_data=login['message'],expect_data=expect['message']
                   ,doc=expect['message'])


if __name__=="__main__":
    pytest.main(['-s',r'D:\iShow\代码\i-show-jiekou-automation9\case\test_dan_api\test_admin_dan\test_login.py'])
