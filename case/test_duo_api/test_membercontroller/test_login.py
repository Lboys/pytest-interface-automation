import sys
from common.assert_api import assert_api
import allure
import pytest
from common.logger import Logger
sys.path.append("..")
from common.read_yaml import ReadYamlConfigs, ReadYamlData
membercontroller = ReadYamlData("duo_data/admin_data/membercontroller.yml").get_yaml_data()#读取数据
data = membercontroller['login']

# @allure.feature(‘测试百度搜索模块’) — 测试模块
# @allure.story(‘百度搜索功能’) —子功能,测试用例函数
# @allure.step(“测试步骤”) ----引用某个函数作为操作步骤的时候使用
# @allure.title(‘用例标题’)

class Test_DengLu():
    @allure.feature('测试membercontroller模块')
    @allure.story('登录功能')
    @allure.step('步骤：正确用户名密码登录')
    @allure.title("登录")
    def test_admin_login(self, membercontroller_z):
        admin_login = membercontroller_z.login()
        print("admin_loginadmin_login",admin_login)
        assert_api(actual_code=admin_login['code'],expect_code=data['except_code']
                   ,actual_data=admin_login['message'],expect_data=data['except_msg']
                   ,doc=data['info'])



if __name__=="__main__":
    pytest.main(['-s',r'D:\daima\pytest-automation-huice\case\test_duo_api\test_admin_duo\test_admin_login.py'])