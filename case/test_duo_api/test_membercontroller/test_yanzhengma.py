import sys
from common.assert_api import assert_api
import allure
import pytest
from common.logger import Logger
sys.path.append("..")
from common.read_yaml import ReadYamlConfigs, ReadYamlData
membercontroller = ReadYamlData("duo_data/admin_data/membercontroller.yml").get_yaml_data()#读取数据
data = membercontroller['get_yanzhengma']

@allure.feature('获取验证码')#测试报告显示测试功能
class Test_DengLu():

    @allure.title("获取验证码")
    @allure.step('随机手机号-获取验证码')#测试报告显示步骤
    def test_admin_login(self, membercontroller_z):
        get_yanzhengma = membercontroller_z.get_yanzhengma()
        print("get_yanzhengma",get_yanzhengma)
        assert_api(actual_code=get_yanzhengma['code'],expect_code=data['except_code']
                   ,actual_data=get_yanzhengma['message'],expect_data=data['except_msg']
                   ,doc=data['info'])



if __name__=="__main__":
    pytest.main(['-s',r'D:\daima\pytest-automation-huice\case\test_duo_api\test_admin_duo\test_admin_login.py'])