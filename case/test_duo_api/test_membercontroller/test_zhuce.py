import sys
from common.assert_api import assert_api
import allure
import pytest
from common.logger import Logger
sys.path.append("..")
from until.phone import TestFaker
from common.read_yaml import ReadYamlConfigs, ReadYamlData
membercontroller = ReadYamlData("duo_data/admin_data/membercontroller.yml").get_yaml_data()#读取数据
data = membercontroller['zhuce']

@allure.feature('注册')#测试报告显示测试功能
class Test_DengLu():

    @allure.title("注册")
    @allure.step('随机手机号-注册')#测试报告显示步骤
    def test_admin_login(self, membercontroller_z):
        phone = TestFaker().get_phone_number()
        get_yanzhengma = membercontroller_z.get_yanzhengma(telephone=phone)
        print("get_yanzhengma",get_yanzhengma)
        yzm = get_yanzhengma['data']
        zhuce = membercontroller_z.zhuce(authCode=yzm,telephone=phone,username="admin"+phone)
        print("zhuce:",zhuce)
        assert_api(actual_code=zhuce['code'],expect_code=data['except_code']
                   ,actual_data=zhuce['message'],expect_data=data['except_msg']
                   ,doc=data['info'])



if __name__=="__main__":
    pytest.main(['-s',r'D:\daima\pytest-automation-huice\case\test_duo_api\test_admin_duo\test_admin_login.py'])