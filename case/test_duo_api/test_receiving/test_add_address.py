import sys

import allure
import pytest

from common.assert_api import assert_api
from common.logger import Logger
sys.path.append("..")
from common.read_yaml import ReadYamlConfigs, ReadYamlData
receiving = ReadYamlData("duo_data/admin_data/receiving.yml").get_yaml_data()#读取数据
data = receiving['add_shipping_address']

@allure.feature('总后台——登录')#测试报告显示测试功能
class Test_DengLu():

    @allure.title("总后台——登录")
    @allure.step('账号，密码正常登录')#测试报告显示步骤
    def test_admin_login(self, receiving_l):
        """显示所有收货地址"""
        address_list = receiving_l.address_list()
        print("address_list",address_list)
        print(address_list['data'][0]['id'])
        id = address_list['data'][0]['id']
        """删除收货地址"""
        address_delete = receiving_l.address_delete(id=id)
        print("address_delete:",address_delete)
        """添加收货地址"""
        add_shipping_address = receiving_l.add_shipping_address()
        assert_api(actual_code=add_shipping_address['code'],expect_code=data['except_code']
                   ,actual_data=add_shipping_address['message'],expect_data=data['except_msg']
                   ,doc=data['info'])




if __name__=="__main__":
    pytest.main(['-s',r'D:\daima\pytest-automation-huice\case\test_duo_api\test_admin_duo\test_admin_login.py'])