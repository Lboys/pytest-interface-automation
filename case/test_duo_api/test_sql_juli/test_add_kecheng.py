


"""sql举例"""
# import sys
# import allure
# import pytest
# from common.logger import Logger
# from api.admin_api import *
# data1 = ReadYaml_anjiekou("duo_data/admin_data/2,course_data.yml").get_yaml_data()#读取数据
# data_add_course = data1['add_course']
# data_unshelve_course = data1['unshelve_course']
#
#
#
# # @pytest.mark.usefixtures("del_course")
# @allure.feature('总后台——课程')#测试报告显示测试功能
# class Test_add_kecheng():
#     # @pytest.mark.run(order=1)
#     # @pytest.mark.admin
#     @allure.title("总后台——新增课程")#标题
#     @allure.step('添加-英语-道长课程')#测试报告显示步骤
#     def test_add_kecheng(self,admin_action):
#         ##z总后台-新增英语课程
#         Logger.info("*************** {}开始执行用例 ***************".format(data_add_course['title']))
#         admin_add_kecheng = admin_action.add_kecheng(name="道长课程",teacher="道长")
#         print("ac_admin_add_kecheng",admin_add_kecheng)
#         try:
#             pytest.assume(admin_add_kecheng['status_code'] == 200)
#             assert admin_add_kecheng['data']['code'] == data_add_course['except_code']
#             assert admin_add_kecheng['data']['message'] == data_add_course['except_msg']
#         except AssertionError as e:
#             Logger.error('=====>{}用例执行未通过，失败信息{}'.format(data_add_course['title'], admin_add_kecheng))
#             # 将异常抛出
#             raise e
#         Logger.info("*************** {}结束执行用例 ***************".format(data_add_course['title']))
#
# if __name__=="__main__":
#     pytest.main(['-s',r'D:\iShow\代码\i-show-jiekou-automation9\case\test_duo_api\test_sql_juli\test02_add_kecheng.py'])