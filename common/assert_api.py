import allure
import pytest
from common.logger import Logger
def assert_api(actual_code ='',expect_code= ''
            ,actual_data ='',expect_data= ''
            ,doc=''):
    Logger.info("*************** {}开始执行用例 ***************".format(doc))
    try:
        assert actual_code == expect_code
        assert actual_data == expect_data
    except AssertionError as e:
        Logger.error('=====>{}用例执行未通过'.format(doc))
        # 将异常抛出
        raise e
    Logger.info("*************** {}结束执行用例 ***************".format(doc))