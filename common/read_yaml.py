import os
import sys
from common.readCofig import Environ
import yaml
class ReadYamlConfigs():
    def __init__(self,filename):
        #切换环境变量
        #test测试环境
        #pre预发布环境

        self.filepath = os.path.abspath(os.path.dirname(os.path.dirname(__file__)) + Environ)+"/"+filename#拼接定位到data文件夹

    def get_yaml_data(self):
        with open(self.filepath, "r", encoding="utf-8")as f:
            # 调用load方法加载文件流
            return yaml.load(f,Loader=yaml.FullLoader)

class ReadYamlData():
    def __init__(self,filename):
        self.filepath = os.path.abspath(os.path.dirname(os.path.dirname(__file__)) + r"/data")+"/"+filename#拼接定位到data文件夹

    def get_yaml_data(self):
        with open(self.filepath, "r", encoding="utf-8")as f:
            # 调用load方法加载文件流
            return yaml.load(f,Loader=yaml.FullLoader)

# if __name__ == '__main__':
#     data = ReadYaml("data.yml").get_yaml_data()
#     data1 = ReadYaml_anjiekou("duo_data/admin_data/1,login_data.yml").get_yaml_data()  # 读取数据
#     print(data)
#     print(data1['login'])


